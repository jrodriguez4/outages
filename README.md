# CENIC Outage Reports

## Cloning this repo

Install git: https://git-scm.com/download/mac

Open a terminal, navigate to your Documents folder, and clone this repo:

    cd ~/Documents
    git clone https://gitlab.com/cenic-cnorman/weekly-outage-reports.git

## First and second pipeline stages

These are described in Twiki.

## Using report-to-sheet.py

The third stage of this pipeline creates a Comma Seperated Value (CSV) sheet
from the plaintext reports generated and filled in earlier.

To accomplish this, invoke the script like so:

    python report-to-sheet.py startdate-enddate.csv report.txt

Where `startdate-enddate.csv` is the name of the CSV sheet to create and
`report.txt` is the plain text report in a text file. Multiple reports can be
combined into one sheet:

    python report-to-sheet.py longer-sheet.csv report1.txt report2.txt

## Using the R script

Download R, RStudio, and install necessary R packages:

* https://cran.r-project.org/bin/macosx/
* https://www.rstudio.com/products/rstudio/download/
* In RStudio console, run:

`install.packages(c("dplyr", "tidyr", "ggplot2", "viridis", "knitr", "printr", "lmtest"))`

Sheets belong in the data folder, and you must ask the trainee for access
to the google drive folder where they are stored.

When you add a new week of data:

* use the report-to-sheet script as above
* name the new sheet ShortMonthDay-ShortMonthDay.csv, e.g. dec20-dec27.csv
* make sure the sheet is in the data folder only
* update the constants.R script to use the new sheet (Cmd + F "UPDATE weekly")
* save the constants.R script
* run the sheet-analysis.R script
* the new master sheet will be written to `data/master.csv`
* upload the new weekly and master sheets to google drive
* test
