from __future__ import print_function

import sys
import os
import re
import csv

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# usage:
#
# Print usage message

def usage():
    eprint("Usage:", os.path.basename(sys.argv[0]), "OUTPUT INPUT1 [INPUT2...]")

# append_ticket:
#
# @sheet:    CSV sheet to append to
# @category: category of ticket being appended
# @number:   number of ticket being appended
# @contents: contents of ticket being appended
#
# Returns: void

def append_ticket(sheet, category, number, contents):
    eprint("Appending Ticket:", number)

    row = [category.strip(), number.strip()]

    for field_re in FIELD_RES:
        field = field_re.search(contents)
        if field is None:
            row.append('')
        else:
            row.append(field.group(1).strip())

    sheet.writerow(row)
    return

# append_category:
#
# @sheet:    CSV writer object to use
# @category: category being appended to the sheet
# @contents: string containing text in category being appended
#
# Returns: void

def append_category(sheet, category, contents):
    eprint("Appending Category:", category)
    ticket = TICKET_RE.search(contents)
    while ticket is not None:
        rt_num = ticket.group(1)
        start_pos = ticket.end()
        ticket = TICKET_RE.search(contents, start_pos)
        if ticket is None:
            end_pos = len(contents) - 1
        else:
            end_pos = ticket.end()
        append_ticket(sheet, category, rt_num, contents[start_pos:end_pos])
    return

# append_reports:
#
# @sheet:   CSV sheet to append report to
# @reports: report files to append to the sheet
#
# Returns: void

def append_reports(sheet, reports):
    for file in reports:
        report = open(file, 'r').read()
        # find the first category in the report
        category = CATEGORY_RE.search(report)
        while category is not None:
            category_id = category.group(1)
            contents_start = category.end()
            # find the next category to know where to split the string
            category = CATEGORY_RE.search(report, contents_start)
            if category is None:
                contents_end = len(report) - 1
            else:
                contents_end = category.start()
            append_category(sheet, category_id, report[contents_start:contents_end])
    return

# append_header:
#
# @sheet:  CSV sheet to append header to
#
# Returns: void

def append_header(sheet):
    columns = ['Category', 'TicketNumber'] + FIELDS
    sheet.writerow(columns)
    return

# report-to-sheet:
#
# @OUTPUT:    file to write CSV sheet to
# @INPUTS...: file(s) to read reports from
#
# Creates a CSV sheet with information contained in the weekly outage reports

if __name__ == '__main__':
    if len(sys.argv) < 3:
        usage()
        sys.exit(2)

    CATEGORY_RE = re.compile("\d\d?\.\s+(.*)\n")
    TICKET_RE = re.compile("RT:\s+(\d*)\n")
    FIELDS = ['SCOPE', 'START', 'END', 'DURATION', 'DIVERSITY', 'CONNECTIVITY',
              'PROVIDER', 'REASON']
    FIELD_RES = []
    for field in FIELDS:
        field_re = re.compile("\s*" + field + ":\s*(.*)\n?")
        if field_re is None:
            eprint("Could not compile regex for", field, "field")
            sys.exit(1)
        else:
            FIELD_RES.append(field_re)

    output = open(sys.argv[1], 'wb') # python2 uses bytes, for py3 use 'w'
    sheet = csv.writer(output, quoting=csv.QUOTE_ALL)

    append_header(sheet)
    append_reports(sheet, sys.argv[2:])
