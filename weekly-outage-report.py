from __future__ import print_function
from bs4 import BeautifulSoup
import urllib2
import subprocess
import getpass
import re
import sys
import os
import datetime

categories = ['Backbone', 'Multi-site', 'UC', 'CSU', 'CC', 'K12/COE', 'Library', 'Other']

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# get_rt:
#
# Prompt users for tickets from ops-analysis email
# Returns: list of ticket numbers

def get_rt():
    eprint('Paste tickets from ops-analysis email (input a blank line to end):')
    pattern = '^(.*[#])([0-9]*)(.*)$'
    matcher = re.compile(pattern)
    rt_num = []
    line = raw_input()
    while(len(line) > 0):
        result = matcher.search(line)
        if result:
            rt_num.append(result.group(2))
        line = raw_input()
    return rt_num

# calculate_duration:
#
# @start: string of start date/time
# @end: string of end date/time
#
# Returns: duration string with format HH:MM, or blank string on error

def calculate_duration(start, end):
    if start == '' or end == '':
        return ''

    datetime_format_string = "%H:%M %Z, %a %m/%d"

    try:
        start_date = datetime.datetime.strptime(start, datetime_format_string)
        end_date = datetime.datetime.strptime(end, datetime_format_string)
    except:
        eprint("Could not parse START or END values")
        return ''

    minutes = (end_date - start_date).total_seconds() / 60
    return "{0:02.0f}:{1:02.0f}".format(minutes // 60, minutes % 60)

# fetch_ticket:
#
# @rt_num: ticket number to retrieve
#
# Returns: raw HTML of the ticket specified

def fetch_ticket(rt_num):
    ticket_url = BASE_URL + rt_num
    try:
        return subprocess.check_output(['curl', '--data', DATA_ARG, ticket_url])
    except:
        eprint("[WARNING] could not fetch ticket: " + rt_num)

# parse_loss:
#
# @message: message to parse
# @regex: regular expression to search for
# 
# Returns: any lines that follow the regex until a blank line is found

def parse_loss(message, regex):
    output = ''
    loss_search = regex.search(message)

    if loss_search is None:
        return output

    for line in message[loss_search.end():].split('\n'):
        if line is '' or LOSSBREAK_RE.search(line):
            break
        output += "{} ;; ".format(line.strip())

    return output

# parse_message:
#
# @message: text to search in
#
# Searches for scope, start, and end fields in the message.
# Calculates duration if start and end fields are found.
#
# Returns: a string with identified or blank fields

def parse_message(message):
    output = ''

    scope = SCOPE_RE.search(message)
    if scope is not None:
        eprint("Found scope expression in message")
        scope_str = scope.group(2)
    else:
        scope_str = ''
    output += "    SCOPE: {}\n".format(scope_str)

    start = START_RE.search(message)
    if start is not None:
        eprint("Found start expression in message")
        start_str = start.group(2)
    else:
        start_str = ''
    output += "    START: {}\n".format(start_str)

    end = END_RE.search(message)
    if end is not None:
        eprint("Found end expression in message")
        end_str = end.group(2)
    else:
        end_str = ''
    output += "    END: {}\n".format(end_str)

    duration_str = calculate_duration(start_str, end_str)
    output += "    DURATION: {}\n".format(duration_str)

    diversity_str = parse_loss(message, DIVERSITY_RE)
    if diversity_str is not '':
        output += "    DIVERSITY: {}\n".format(diversity_str)

    connectivity_str = parse_loss(message, CONNECTIVITY_RE)
    if connectivity_str is not '':
        output += "    CONNECTIVITY: {}\n".format(connectivity_str)

    return output

# parse_ticket:
#
# @rt_num: ticket number to fetch and parse
#
# Fetches the HTML for a ticket and converts to a soup object,
# then iterates over the comments found in the ticket and calls
# parse_message() on those that contain the 'RESOLVED' string.
#
# NOTE: this function will behave differently if the RT_REVERSED
#       environment variable is set to '1'. In such cases, it will
#       assume the first RESOLVED comment is the most recent, rather
#       than the default of using the last comment as the most recent.
#
# Returns: string with identified or blank fields, or a blank string on error

def parse_ticket(rt_num):
    parsed = ''
    html = fetch_ticket(rt_num)
    if html is None:
        return ''
    soup = BeautifulSoup(html, 'html.parser')
    for body in soup.find_all(True, attrs={'class': 'message-stanza-depth-0'}):
        message = body.get_text(separator='\n')
        if RESOLVED_RE.search(message) is not None:
            eprint('Found resolved message')
            parsed = parse_message(message)
            if os.getenv("RT_REVERSED") == "1":
                eprint("Reverse option in use, first resolved message is most recent")
                return parsed
    return parsed

# outage_info:
#
# @rt_num: ticket number to produce a template for
# @full: whether to include SCOPE/START/END/DURATION fields
#
# Returns: template string for the RT number given

def outage_info(rt_num, full=True):
    outage = 'RT: {}\n'.format(rt_num)

    if full:
        result = parse_ticket(rt_num)
        if result == '':
            for field in ["SCOPE", "START", "END", "DURATION"]:
                outage += "    " + field + ": \n"
        else:
            outage += result

    for field in ["PROVIDER", "REASON"]:
        outage += "    " + field + ": \n"

    return outage

# heading:
#
# @start: string to use as the start date
# @end: string to use as the end date
#
# Returns: a heading string for the weekly outage report

def heading(start, end):
    left_dashes = 34
    right_dashes = 31
    return '-' * left_dashes + \
    ' Weekly Outage Report {d1} - {d2} '.format(d1=start, d2=end) + \
    '-' * right_dashes + '\n' * 2

# outage_counts:
#
# @total: total number of tickets in report
#
# Returns: string with unfilled category totals field

def outage_counts(total):
    return 'Total Outages: {}'.format(total) + '\n\n' + ':\n'.join(categories) + ':\n\n'

# outage_categories:
#
# Returns: string with outage categories used in report

def outage_categories():
    count = 0
    output = '\n'
    for cat in categories:
        count += 1
        output += "{}. {}\n\n".format(count, cat)
    return output

# create_template:
#
# @rt_nums: list of ticket numbers to compile report from
#
# Prompts user for start/end dates of report, and creates a template
# from the tickets specified. Writes the template to a file.
#
# Returns: void
        
def create_template(rt_nums):
    start_date = raw_input('Start date of report (MM/DD/YY): ')
    end_date = raw_input('End date of report (MM/DD/YY): ')
    header = heading(start_date, end_date)
    outages = '\n'.join([outage_info(rt) for rt in rt_nums])
    segments = outage_categories()
    counts = outage_counts(len(rt_nums))

    fname = 'weekly_outage_{}_{}.txt'.format(start_date, end_date)
    fname = fname.replace('/','_')

    if os.path.exists(fname):
        prompt = raw_input('file {} exists. Overwrite (Y/n)? '.format(fname))
    else:
        prompt = 'Y'
    if re.match('^Y\w*$', prompt, flags = re.I):
        with open(fname, 'w+') as f:
            f.write(header)
            f.write(counts)
            f.write(outages)
            f.write(segments)

    return

# weekly-outage-report:
#
# Writes a template file and opens Google Chrome based on tickets entered.

if __name__ == '__main__':
    BROWSER = '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome'
    BASE_URL = 'https://rt.engineering.cenic.org/Ticket/Display.html?id='
    RESOLVED_RE = re.compile("^(Summary of RESOLVED outage:)", re.MULTILINE)
    SCOPE_RE = re.compile("(SCOPE:)\s+(.*)")
    START_RE = re.compile("(START:)\s+(.*)")
    END_RE   = re.compile("(END:)\s+(.*)")
    DIVERSITY_RE = re.compile("(LOSS OF DIVERSITY FOR:\n)")
    CONNECTIVITY_RE = re.compile("(LOSS OF CONNECTIVITY FOR:\n)")
    LOSSBREAK_RE = re.compile("(COMMENTS:|IMPACTED XD CLRs:|Layer \d circuits affected:|LOSS OF CONNECTIVITY FOR:)")
    user = raw_input("Username (RT): ")
    password = getpass.getpass()
    DATA_ARG = "user={}&pass={}".format(urllib2.quote(user), \
                                        urllib2.quote(password))
    rt_nums = get_rt()
    create_template(rt_nums)
    if len(rt_nums) > 0:
        args = [BROWSER]
        args.extend(['--new-window'])
        args.extend([BASE_URL + i for i in rt_nums])
        subprocess.call(args, env = os.environ.copy(), shell=False)
